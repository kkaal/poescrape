require 'open-uri'
require 'nokogiri'

doc = Nokogiri.HTML(open('https://www.pathofexile.com/forum/view-thread/1841361'))
inline_script = doc.xpath('//script[not(@src)]')
inline_script.each do |script|
  puts '-' * 50, script.text
end
