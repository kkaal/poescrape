# Poescrape

The purpose of this gem is to gather information from Path of Exile player SSF HC ladder ("https://www.pathofexile.com/forum/view-thread/1841361"). At the very least the ladder scraper was suppose to extract the rank in the leaderboard, player name and the level of the character. 

Building a basic web scraper wasn't too hard. The problems arose when I realised the nokogiri gem can't simply be used to parse needed information because it doesn't see javascript. So all the needed information was unaccessable.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'poescrape'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install poescrape

## Usage

Usage very limited due to not being able to finish the project and meet the requirements.

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake test` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/poescrape.

